/* 1. Опишіть, як можна створити новий HTML тег на сторінці.

За допомогою document.createElement

Його формат: var element = document.createElement(tagName, [options]);


2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

insertAdjacentHTML() розбирає вказаний текст як HTML або XML і вставляє отримані вузли (nodes) у DOM дерево у вказану позицію.

targetElement.insertAdjacentHTML(position, text);

position

     DOMString - визначає позицію елемента, що додається щодо елемента, що викликав метод. 
	 Повинно відповідати одному з наступних значень (чутливо до регістру):

         'beforebegin': до самого element (до відкриваючого тегу).
         'afterbegin': відразу після відкриваючого тегу element (перед першим нащадком).
         'beforeend': відразу перед закриваючим тегом element (після останнього нащадка).
         'afterend': після element (після закриваючого тегу)

3. Як можна видалити елемент зі сторінки?

Можна видалити за допомогою метода remove()

*/

const arrayCities = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let html = '<ul>';
arrayCities.forEach(function (item, i, arr, parent = document.body) {
  html += '<li>' + item + '</li>';
});
html += '</ul>';
document.write(html);
